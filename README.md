# Commerce Bangkok Bank iPay

Sell your goods using [www.bangkokbank.com](http://www.bangkokbank.com/BangkokBank/PersonalBanking/DailyBanking/CreditCards/BualuangiPay/Pages/default.aspx) as a payment gateway.

## Dependencies

- [Autoload](https://www.drupal.org/project/autoload)
  Loads all OO stuff for you.
- [Elysia Cron](https://www.drupal.org/project/elysia_cron)
  Checks incomplete payments, when a customer hasn't come back to the website.
- [Commerce Utilities](https://www.drupal.org/project/commerce_utils)
  Unified, object-oriented interface for building payment methods.

## Usage

Configure the payment method at `Store » Configuration » Payment methods » Bangkok Bank iPay` (`admin/commerce/config/payment-methods/manage/commerce_payment_commerce_bangkokbank`).

![Bangkok Bank iPay settings](docs/images/settings.png)

### Pending authorisations

To see the list of uncompleted payments you have to enable the `commerce_payment_watcher_ui` and clear the cache. After this it will appears at `Store » Configuration » Payment methods » Bangkok Bank iPay pending authorisations` (`admin/commerce/config/payment-methods/payment-watchers/commerce-bangkokbank`).

Moreover, there you'll be able to forcibly check payment status using an appropriate action.

![Bangkok Bank pending authorisations](docs/images/ui.png)
