<?php

/**
 * @file
 * Commerce integration.
 */

use Commerce\Utils\PaymentPlugin;
use Drupal\commerce_bangkokbank\Entity\PaymentWatcher;
use Drupal\commerce_bangkokbank\Payment\Notification;
use Drupal\commerce_bangkokbank\Payment\NotificationController;
use Drupal\commerce_bangkokbank\Payment\Form\SettingsForm;
use Drupal\commerce_bangkokbank\Payment\Form\SubmitForm;
use Drupal\commerce_bangkokbank\Payment\Form\RedirectForm;
use Drupal\commerce_bangkokbank\Payment\Capture\CaptureProcessor;
use Drupal\commerce_bangkokbank\Payment\Authorisation\Request;
use Drupal\commerce_bangkokbank\Payment\Authorisation\Response;
use Drupal\commerce_bangkokbank\Payment\PaymentTypeInterface;
use Drupal\commerce_bangkokbank\Payment\Transaction\Payment;
use Drupal\commerce_bangkokbank\Payment\Transaction\Refund;

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_bangkokbank_commerce_payment_method_info() {
  $info = [];

  $info[COMMERCE_BANGKOKBANK_PAYMENT_METHOD] = [
    'title' => 'Bangkok Bank iPay',
    'short_title' => 'iPay',
    'description' => t('Redirect users to submit payments through iPay of Bangkok Bank.'),
    'active' => TRUE,
    'offsite' => TRUE,
    'terminal' => FALSE,
    'offsite_autoredirect' => TRUE,
    'payment_plugin' => (new PaymentPlugin())
      ->setSettingsFormClass(SettingsForm::class)
      ->setSubmitFormClass(SubmitForm::class)
      ->setRedirectFormClass(RedirectForm::class)
      ->setAuthorisationRequestClass(Request::class)
      ->setAuthorisationResponseClass(Response::class)
      ->setPaymentTransactionClass(Payment::class)
      ->setRefundTransactionClass(Refund::class)
      ->setNotificationsController(NotificationController::class)
      ->setPaymentWatcherEntityClass(PaymentWatcher::class)
      ->setPaymentCaptureProcessor(CaptureProcessor::class),
  ];

  return $info;
}

/**
 * Implements hook_commerce_bangkokbank_notification().
 */
function commerce_bangkokbank_commerce_bangkokbank_notification($event, \stdClass $order, Notification $notification) {
  /* @var \Drupal\commerce_bangkokbank\Payment\Transaction\Payment $transaction */
  $transaction = commerce_utils_get_transaction_instance(COMMERCE_BANGKOKBANK_PAYMENT_METHOD_INSTANCE, 'payment', $order);
  $transaction->setPayload($notification);

  if ($notification->isSuccessful()) {
    $transaction->authorise($notification->getPaymentReference());

    // The transaction needs to be finalized at this point since payment
    // workflow does not support authorisations and becomes accepted at once.
    if (PaymentTypeInterface::NORMAL === $transaction->getPaymentMethod()['settings']['payment_type']) {
      $transaction->finalize();
    }

    commerce_payment_redirect_pane_next_page($order);
  }
  else {
    $transaction->fail($notification->getPaymentReference());

    commerce_payment_redirect_pane_previous_page($order);
  }

  $transaction->save();

  PaymentWatcher::deleteForOrder($order, COMMERCE_BANGKOKBANK_PAYMENT_METHOD);
}
