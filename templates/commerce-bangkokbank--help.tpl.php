<?php

/**
 * @file
 * Help page.
 */

use Drupal\commerce_bangkokbank\BankReturnCodesDescription;
?>

<ul>
  <li>
    <h4><?php print t('How does the notification system work?'); ?></h4>

    <p>
      <?php print t('Once the gateway gets a payment it sends an immediate notification (DataFeed) to configured endpoint (URL can be set by contacting support team). The data comes as a POST-request and has primary and secondary response codes of a bank, based on which a state of the transaction can be recognized. The notification can be received only once for every particular payment. After receiving it you can do an API call to get detailed information about the payment.'); ?>
    </p>
    <p>
      <?php print t('You are free to do an additional API call to capture the funds from a customer account (no notification afterwards) as soon as you will gather the details and treat the payment as successful (only when having an intermediate step in payment acceptance scenario - "hold" payment mode).'); ?>
    </p>
  </li>

  <li>
    <h4><?php print t('Descriptions of codes, returned by gateway'); ?></h4>

    <?php foreach ([
      [
        '#title' => t('Primary response codes'),
        'table' => [
          '#theme' => 'table',
          '#header' => [t('Code'), t('Description')],
          '#rows' => BankReturnCodesDescription::PRIMARY,
        ],
      ],
      [
        '#title' => t('Response codes combinations'),
        '#description' => t('Known combinations of primary and secondary codes, returning by the bank.'),
        '#attributes' => [
          'class' => ['collapsed', 'collapsible'],
        ],
        'table' => [
          '#theme' => 'table',
          '#header' => [t('Primary'), t('Secondary'), t('Description')],
          '#rows' => BankReturnCodesDescription::COMBINATIONS,
        ],
      ],
    ] as $fieldset): ?>
      <?php
        $element = $fieldset + [
          '#type' => 'fieldset',
          '#attached' => [
            'library' => [
              ['system', 'drupal.collapse'],
            ],
          ],
        ];

        print drupal_render($element);
      ?>
    <?php endforeach; ?>
  </li>
</ul>
