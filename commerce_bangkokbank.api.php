<?php

/**
 * @file
 * Commerce Bangkok Bank API.
 */

/**
 * {@inheritdoc}
 *
 * @see hook_PAYMENT_METHOD_payment_authorisation_request_alter()
 */
function hook_commerce_bangkokbank_payment_authorisation_request_alter(\Drupal\commerce_bangkokbank\Payment\Authorisation\Request $payment, \stdClass $order, array $payment_method) {

}

/**
 * {@inheritdoc}
 *
 * @see hook_PAYMENT_METHOD_payment_authorisation_response_alter()
 */
function hook_commerce_bangkokbank_payment_authorisation_response_alter(\Drupal\commerce_bangkokbank\Payment\Authorisation\Response $payment, \stdClass $order, array $payment_method) {

}

/**
 * React on response of API query.
 *
 * @see \Commerce\Utils\Payment\Action\ActionBase::request()
 * @see \Drupal\commerce_bangkokbank\Payment\Action\Query
 */
function hook_commerce_bangkokbank_query_received(\Drupal\commerce_bangkokbank\Payment\Transaction\Payment $transaction, \stdClass $order, \Drupal\commerce_bangkokbank\Payment\Action\QueryResponse $response) {

}

/**
 * React on response of API query.
 *
 * @see \Commerce\Utils\Payment\Action\ActionBase::request()
 * @see \Drupal\commerce_bangkokbank\Payment\Action\Query
 */
function hook_commerce_bangkokbank_query_rejected(\Drupal\commerce_bangkokbank\Payment\Transaction\Payment $transaction, \stdClass $order, \Drupal\commerce_bangkokbank\Payment\Action\QueryResponse $response) {

}

/**
 * React on response of capturing.
 *
 * @see \Commerce\Utils\Payment\Action\ActionBase::request()
 * @see \Drupal\commerce_bangkokbank\Payment\Action\Capture
 */
function hook_commerce_bangkokbank_capture_received(\Drupal\commerce_bangkokbank\Payment\Transaction\Payment $transaction, \stdClass $order, \Drupal\commerce_bangkokbank\Payment\Action\CaptureResponse $response) {

}

/**
 * React on response of capturing.
 *
 * @see \Commerce\Utils\Payment\Action\ActionBase::request()
 * @see \Drupal\commerce_bangkokbank\Payment\Action\Capture
 */
function hook_commerce_bangkokbank_capture_rejected(\Drupal\commerce_bangkokbank\Payment\Transaction\Payment $transaction, \stdClass $order, \Drupal\commerce_bangkokbank\Payment\Action\CaptureResponse $response) {

}

/**
 * {@inheritdoc}
 *
 * IMPORTANT: Notification comes immediately after transaction becomes
 * processed by the gateway. There is no separation between notification
 * types and no possibility to distribute their sending between different
 * URLs/domains.
 *
 * @param string $event
 *   The value always will be "notification".
 *
 * @see \Drupal\commerce_bangkokbank\Payment\NotificationController::getEvent()
 * @see hook_PAYMENT_METHOD_notification()
 */
function hook_commerce_bangkokbank_notification($event, \stdClass $order, \Drupal\commerce_bangkokbank\Payment\Notification $notification) {

}
