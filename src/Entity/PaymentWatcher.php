<?php

namespace Drupal\commerce_bangkokbank\Entity;

use Drupal\commerce_bangkokbank\Payment\Action\Query;
use Drupal\commerce_bangkokbank\Payment\PaymentStatusInterface;
use Drupal\commerce_payment_watcher\Entity\PaymentWatcherEntity;

/**
 * {@inheritdoc}
 */
class PaymentWatcher extends PaymentWatcherEntity {

  /**
   * {@inheritdoc}
   */
  public function checkPaymentStatus() {
    $query = new Query($this->order);

    if (!$query->isAvailable()) {
      throw new \RuntimeException(t('Payment query is not available for "@label".', [
        '@label' => $this->label(),
      ]));
    }

    if (!$query->request()) {
      throw $this->paymentNotFound();
    }

    $payment = $query->getResponse();

    if (!$payment->isSuccessful()) {
      throw new \RuntimeException(t('Request to gateway was not successful and ended with a message: %message.', [
        '%message' => $payment->getMessage(),
      ]));
    }

    // Payments are identified by order number and email of a user who made it.
    /* @see \Drupal\commerce_bangkokbank\Payment\Authorisation\Request::__construct() */
    /* @see \Drupal\commerce_bangkokbank\Payment\Authorisation\Response::__construct() */
    if (
      $payment->getAdditionalMerchantReference(1) !== $this->order->mail &&
      $payment->getAdditionalMerchantReference(2) !== $this->order->created
    ) {
      throw $this->paymentNotFound();
    }

    switch ($payment->getPaymentStatus()) {
      case PaymentStatusInterface::REJECTED:
        $transaction = $query->getTransaction();
        $transaction->fail($payment->getPaymentReference());
        $transaction->setPayload($payment);
        $transaction->save();

        // Self-destruction.
        $this->delete();
        break;

      // This response will happen only in case hold payment mode.
      case PaymentStatusInterface::AUTHORISED:
        $transaction = $query->getTransaction();
        $transaction->authorise($payment->getPaymentReference());
        $transaction->setPayload($payment);
        $transaction->save();

        commerce_payment_redirect_pane_next_page($this->order);

        // Self-destruction.
        $this->delete();
        break;

      // This response will happen only in case of normal payment mode.
      case PaymentStatusInterface::ACCEPTED:
        $transaction = $query->getTransaction();

        if (!$transaction->isAuthorised()) {
          $transaction->authorise($payment->getPaymentReference());
        }

        // In normal mode payment acceptance is not distributed (happens
        // immediately, without notifications process).
        $transaction->finalize();
        $transaction->setPayload($payment);
        $transaction->save();

        commerce_payment_redirect_pane_next_page($this->order);

        // Self-destruction.
        $this->delete();
        break;

      default:
        $this->saveAttempt();
    }
  }

}
