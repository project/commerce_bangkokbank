<?php

namespace Drupal\commerce_bangkokbank\Payment;

/**
 * Lists 3D-secure authorisation statuses.
 *
 * All values of constants MUST BE in lowercase!
 *
 * @see \Drupal\commerce_bangkokbank\Payment\Action\QueryResponse::getPayerAuth()
 */
interface PaymentAuthenticationInterface {

  /**
   * Card is 3D-secure enrolled and authentication succeeds.
   */
  const SUCCEEDED = 'y';
  /**
   * Card is 3D-secure enrolled but authentication fails.
   */
  const FAILED = 'n';
  /**
   * Card is 3D-secure enrolled and check is pending.
   */
  const PENDING = 'p';
  /**
   * Card is not 3D-secure enrolled yet.
   */
  const DISABLED = 'a';
  /**
   * Card is 3D-secure enrolled but check has not been processed.
   */
  const UNPROCESSED = 'u';

}
