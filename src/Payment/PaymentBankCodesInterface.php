<?php

namespace Drupal\commerce_bangkokbank\Payment;

/**
 * Interface for describing objects containing bank's return codes.
 *
 * @see \Drupal\commerce_bangkokbank\BankReturnCodesDescription
 */
interface PaymentBankCodesInterface {

  /**
   * Returns primary status code, returned by bank.
   *
   * @return int
   *   Primary status code of payment.
   */
  public function getPrimaryBankReturnCode();

  /**
   * Returns secondary status code, returned by bank.
   *
   * @return int
   *   Secondary status code of payment.
   */
  public function getSecondaryBankReturnCode();

  /**
   * Returns string or instance of an object, representing bank codes.
   *
   * @return string|\Drupal\commerce_bangkokbank\BankReturnCodesDescription
   *   Bank codes description string or instance of the appropriate object.
   */
  public function getBankReturnCodesDescription();

}
