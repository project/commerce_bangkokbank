<?php

namespace Drupal\commerce_bangkokbank\Payment;

use Commerce\Utils\NotificationControllerBase;
use Commerce\Utils\Payment\Exception\NotificationException;

/**
 * Notifications handler.
 *
 * @property \Drupal\commerce_bangkokbank\Payment\Notification $data
 */
class NotificationController extends NotificationControllerBase {

  /**
   * {@inheritdoc}
   */
  const NOTIFICATIONS_PATH = 'commerce/bangkokbank/notification';
  /**
   * {@inheritdoc}
   */
  const NOTIFICATION_WRAPPER = Notification::class;

  /**
   * {@inheritdoc}
   */
  public function getEvent() {
    // Bangkok Bank iPay has no types separation. The notification comes once
    // payment processed by their system.
    return 'notification';
  }

  /**
   * {@inheritdoc}
   */
  public function locateOrder() {
    return $this->data->getMerchantReference();
  }

  /**
   * {@inheritdoc}
   */
  public function handle(\stdClass $order) {

  }

  /**
   * {@inheritdoc}
   */
  public function error(NotificationException $exception) {

  }

  /**
   * {@inheritdoc}
   */
  public function terminate(\Exception $exception) {

  }

  /**
   * {@inheritdoc}
   */
  public function getResponse() {
    print 'OK';
    drupal_exit();
  }

}
