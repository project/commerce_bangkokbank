<?php

namespace Drupal\commerce_bangkokbank\Payment;

/**
 * Lists payment types.
 */
interface PaymentTypeInterface {

  const NORMAL = 'N';
  const HOLD = 'H';

}
