<?php

namespace Drupal\commerce_bangkokbank\Payment;

use Commerce\Utils\NotificationInterface;
use Drupal\commerce_bangkokbank\Payment\Action\QueryResponse;

/**
 * Wraps upcoming DataFeed notification.
 */
class Notification extends QueryResponse implements NotificationInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(\stdClass $data) {
    if (!isset($data->successcode)) {
      throw new \RuntimeException('Invalid notification.');
    }

    $input = [];

    foreach ((array) $data + [
      // Notification doesn't have the following properties.
      /* @see \Drupal\commerce_bangkokbank\Payment\Action\ActionResponse::getMessage() */
      'errMsg' => '',
      /* @see \Drupal\commerce_bangkokbank\Payment\Action\ActionResponse::isSuccessful() */
      'resultCode' => $data->successcode,
      /* @see \Drupal\commerce_bangkokbank\Payment\Action\ActionResponse::getPaymentStatus() */
      'orderStatus' => '',
      // Notification receiving means that DataFeed settled correctly.
      /* @see \Drupal\commerce_bangkokbank\Payment\Action\QueryResponse::isSettled() */
      'settle' => 'T',
      /* @see \Drupal\commerce_bangkokbank\Payment\Action\QueryResponse::getSettleTime() */
      'settleTime' => '',
      /* @see \Drupal\commerce_bangkokbank\Payment\Action\QueryResponse::getSettleBatchId() */
      'settleBatchId' => '',
    ] as $property => $value) {
      // Inbound data in notification quite similar to Query response, except
      // several property names are starts from a capital letter.
      // For instance (notification => query):
      // Ord => ord;
      // Ref => ref;
      // Cur => cur;
      // etc.
      $input[lcfirst($property)] = $value;
    }

    // We've copied this value into "resultCode".
    unset($input['successcode']);

    parent::__construct($input);
  }

}
