<?php

namespace Drupal\commerce_bangkokbank\Payment;

/**
 * Lists payment languages.
 */
interface PaymentLanguageInterface {

  const THAI = 'T';
  const ENGLISH = 'E';
  const CHINESE = 'X';

}
