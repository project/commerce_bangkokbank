<?php

namespace Drupal\commerce_bangkokbank\Payment\Action;

use Commerce\Utils\Payment\Action\ActionBase;
use Drupal\commerce_bangkokbank\Payment\Environment;

/**
 * {@inheritdoc}
 *
 * @todo Implement "void" and "reverse_auth" actions.
 *
 * @property \Drupal\commerce_bangkokbank\Payment\Transaction\Payment $transaction
 * @method \Drupal\commerce_bangkokbank\Payment\Transaction\Payment getTransaction()
 */
abstract class Action extends ActionBase {

  /**
   * Action: API query (must be in lowercase).
   */
  const QUERY = 'query';
  /**
   * Action: Payment capture (must be in lowercase).
   */
  const CAPTURE = 'capture';
  /**
   * Fully qualified path to class to wrap response in.
   */
  const RESPONSE_CLASS = '';

  /**
   * {@inheritdoc}
   */
  protected function buildRequest() {
    $order_wrapper = $this->transaction->getOrder();
    $payment_method = $this->transaction->getPaymentMethod();
    $order_currency = $order_wrapper->commerce_order_total->currency_code->value();
    $currencies = $payment_method['settings'][$payment_method['settings']['mode']]['currencies'];

    if (empty($currencies[$order_currency])) {
      throw new \RuntimeException(t('Order paid with @order_currency currency which is not allowed.', [
        '@order_currency' => $order_currency,
      ]));
    }

    $request = [
      'url' => Environment::createFromSettings($payment_method['settings'])->getApiUrl(),
      'method' => 'POST',
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      'data' => [
        'merchantId' => $currencies[$order_currency]['merchant_id'],
        'loginId' => $currencies[$order_currency]['api']['username'],
        'password' => $currencies[$order_currency]['api']['password'],
        'actionType' => ucfirst($this->action),
      ],
    ];

    switch ($this->action) {
      case self::QUERY:
        $request['data']['orderRef'] = $order_wrapper->order_number->value();
        break;

      case self::CAPTURE:
        $request['data']['payRef'] = $this->transaction->getRemoteId();
        /* @see \Drupal\commerce_bangkokbank\Payment\Authorisation\Request::setPaymentAmount() */
        $request['data']['amount'] = commerce_currency_amount_to_decimal(
          $order_wrapper->commerce_order_total->amount->value(),
          $order_wrapper->commerce_order_total->currency_code->value()
        );
        break;
    }

    return $request;
  }

  /**
   * {@inheritdoc}
   */
  protected function sendRequest($request) {
    $request['data'] = drupal_http_build_query($request['data']);
    $response = drupal_http_request($request['url'], $request);
    $class = static::RESPONSE_CLASS;

    if (isset($response->error)) {
      throw new \RuntimeException(sprintf('Request to payment gateway ended with an error: "%s".', $response->error));
    }

    if (empty($response->data)) {
      throw new \RuntimeException('Request to payment gateway ended with empty response.');
    }

    if (!empty($class) && class_exists($class)) {
      $response = new $class($response->data);

      /* @var \Drupal\commerce_bangkokbank\Payment\Action\ActionResponse $response */
      if (is_subclass_of($response, ActionResponse::class) && !$response->isSuccessful()) {
        throw new \RuntimeException($response->getMessage());
      }
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTransactionType() {
    switch ($this->action) {
      case self::QUERY:
      case self::CAPTURE:
        return 'payment';

      default:
        throw new \InvalidArgumentException(t('The "@action" API action is not supported.', [
          '@action' => $this->action,
        ]));
    }
  }

}
