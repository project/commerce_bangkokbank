<?php

namespace Drupal\commerce_bangkokbank\Payment\Action;

use Commerce\Utils\CreditCard;
use Drupal\commerce_bangkokbank\BankReturnCodesDescription;
use Drupal\commerce_bangkokbank\Payment\PaymentCardDataInterface;
use Drupal\commerce_bangkokbank\Payment\PaymentBankCodesInterface;

/**
 * Base implementation of response wrapper.
 */
abstract class ActionResponse implements \Serializable {

  /**
   * Response representation.
   *
   * @var array
   */
  protected $data = [];

  /**
   * ActionResponse constructor.
   *
   * @param string|array $data
   *   Query response.
   */
  public function __construct($data) {
    if (is_string($data)) {
      $data = trim($data);

      if (stripos($data, '<?xml') === 0) {
        $xml = new \SimpleXMLElement($data);

        // Convert XML into a proper array.
        foreach ((array) (empty($xml->record) ? $xml : $xml->record) as $property => $value) {
          $this->data[$property] = (string) $value;
        }
      }
      else {
        // For instance, а response on the "capture" request - is HTTP query
        // string. Response on the "query" request could be XML in case of
        // successful processing by a gateway or, in the same way, could be
        // HTTP query string when something goes wrong.
        parse_str(rawurldecode($data), $this->data);
      }
    }
    elseif (is_array($data)) {
      $this->data = $data;
    }
    else {
      throw new \InvalidArgumentException(sprintf('Input argument for "%s()" must be string or array, "%s" given.', __METHOD__, gettype($data)));
    }

    // The "resultCode" exists only in a case when HTTP query string is
    // returned by the gateway. So, if we got an XML, then assume that
    // request has been made successfully.
    $this->data += ['resultCode' => 0];

    if ($this instanceof PaymentBankCodesInterface) {
      // Update the value of "resultCode".
      $this->data['resultCode'] = $this->getPrimaryBankReturnCode();
      $this->data['bankReturnCodesDescription'] = (string) new BankReturnCodesDescription($this);
    }

    if ($this instanceof PaymentCardDataInterface) {
      $this->data['paymentMethod'] = (new CreditCard($this->getFirstFourCardNumberDigits()))->getType();
    }
  }

  /**
   * Returns the data as an array.
   *
   * @return array
   *   Action's data.
   */
  public function toArray() {
    return $this->data;
  }

  /**
   * Checks whether response child exists.
   *
   * @param string $name
   *   Name of child to check.
   *
   * @return bool
   *   A state of check.
   */
  public function __isset($name) {
    return isset($this->data[$name]);
  }

  /**
   * Global getter for response child.
   *
   * @param string $name
   *   Name of child to get.
   *
   * @return null|string
   *   Value of child node.
   */
  public function __get($name) {
    if (!isset($this->{$name}) || 'null' === strtolower($this->data[$name])) {
      return NULL;
    }

    return $this->data[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function serialize() {
    return serialize($this->data);
  }

  /**
   * {@inheritdoc}
   */
  public function unserialize($serialized) {
    $this->data = unserialize($serialized);
  }

  /**
   * Returns status whether capture request was made correctly.
   *
   * @return bool
   *   A state of check.
   */
  public function isSuccessful() {
    // Don't worry about converting "NULL" to "0". We are appending the
    // property if it doesn't exist, so "NULL" can be returned by getter
    // only if property actually contains this value.
    return 0 === (int) $this->resultCode;
  }

  /**
   * Returns payment status.
   *
   * @return null|string
   *   One of payment statuses, described in "PaymentStatusInterface".
   *
   * @see \Drupal\commerce_bangkokbank\Payment\PaymentStatusInterface
   */
  public function getPaymentStatus() {
    return strtolower($this->orderStatus);
  }

  /**
   * Returns unique ID of payment on gateway.
   *
   * @return null|string
   *   Unique ID of payment.
   */
  public function getPaymentReference() {
    return $this->payRef;
  }

  /**
   * Returns oder number.
   *
   * @return null|string
   *   Number of commerce order.
   */
  public function getMerchantReference() {
    return $this->ref;
  }

  /**
   * Returns payment amount.
   *
   * @return null|string
   *   Payment amount.
   */
  public function getAmount() {
    return $this->amt;
  }

  /**
   * Returns numeric ID of currency used to pay.
   *
   * @return null|string
   *   Currency ID.
   */
  public function getCurrencyId() {
    return $this->cur;
  }

  /**
   * Returns message with which query was ended.
   *
   * @return null|string
   *   Query status message.
   */
  public function getMessage() {
    // Here will be not only error messages.
    return $this->errMsg;
  }

}
