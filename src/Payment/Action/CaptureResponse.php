<?php

namespace Drupal\commerce_bangkokbank\Payment\Action;

/**
 * Wraps response of capture request.
 */
class CaptureResponse extends ActionResponse {

}
