<?php

namespace Drupal\commerce_bangkokbank\Payment\Action;

/**
 * Query existing payment.
 *
 * @method \Drupal\commerce_bangkokbank\Payment\Action\QueryResponse getResponse()
 */
class Query extends Action {

  /**
   * {@inheritdoc}
   */
  const RESPONSE_CLASS = QueryResponse::class;

  /**
   * {@inheritdoc}
   */
  public function __construct($order) {
    parent::__construct($order, '', self::QUERY);
  }

  /**
   * {@inheritdoc}
   */
  public function isAvailable() {
    // Payment information gathering is always available.
    return TRUE;
  }

}
