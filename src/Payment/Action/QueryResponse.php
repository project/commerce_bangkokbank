<?php

namespace Drupal\commerce_bangkokbank\Payment\Action;

use Drupal\commerce_bangkokbank\Payment\PaymentCardDataInterface;
use Drupal\commerce_bangkokbank\Payment\PaymentBankCodesInterface;

/**
 * Wraps response of API query.
 *
 * @see \Drupal\commerce_bangkokbank\Payment\Action\Query
 */
class QueryResponse extends ActionResponse implements PaymentBankCodesInterface, PaymentCardDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getPrimaryBankReturnCode() {
    return (int) $this->prc;
  }

  /**
   * {@inheritdoc}
   */
  public function getSecondaryBankReturnCode() {
    return (int) $this->src;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\commerce_bangkokbank\Payment\Action\ActionResponse::__construct()
   */
  public function getBankReturnCodesDescription() {
    return $this->bankReturnCodesDescription;
  }

  /**
   * Returns bank reference number.
   *
   * @return null|string
   *   Bank reference number.
   */
  public function getBankReferenceNumber() {
    return $this->ord;
  }

  /**
   * Returns shopper name.
   *
   * @return null|string
   *   Shopper name.
   */
  public function getShopperName() {
    return $this->holder;
  }

  /**
   * Return approval code.
   *
   * @return null|string
   *   Approval code.
   */
  public function getApprovalCode() {
    return $this->authId;
  }

  /**
   * Returns Electronic Commerce Indicator.
   *
   * @return null|string
   *   MasterCard:
   *   - 00: Authentication is unsuccessful or not attempted. The credit card
   *         is either a non-3D card or card issuing bank does not handle it
   *         as a 3D transaction.
   *   - 01: Either cardholder or card issuing bank is not 3D enrolled. 3D card
   *         authentication is unsuccessful, in sample situations as:
   *           1. 3D Cardholder not enrolled.
   *           2. Card issuing bank is not 3D Secure ready.
   *   - 02: Both cardholder and card issuing bank are 3D enabled. 3D card
   *         authentication is successful.
   *   JCB:
   *   VISA:
   *   - 05: Both cardholder and card issuing bank are 3D enabled. 3D card
   *         authentication is successful.
   *   - 06: Either cardholder or card issuing bank is not 3D enrolled. 3D card
   *         authentication is unsuccessful, in sample situations as:
   *           1. 3D cardholder not enrolled.
   *           2. Card issuing bank is not 3D Secure ready.
   *   - 07: Authentication is unsuccessful or not attempted. The credit card
   *         is either a non-3D card or card issuing bank does not handle it as
   *         a 3D transaction.
   */
  public function getElectronicCommerceIndicator() {
    return $this->eci;
  }

  /**
   * Returns payer authentication status.
   *
   * @return null|string
   *   Use constants of "PaymentAuthenticationInterface" for comparison.
   */
  public function getPayerAuth() {
    return strtolower($this->payerAuth);
  }

  /**
   * Returns shopper IP.
   *
   * @return null|string
   *   Shopper IP.
   */
  public function getShopperIp() {
    return $this->sourceIp;
  }

  /**
   * Return country code.
   *
   * @return null|string
   *   Country code.
   */
  public function getCountryCode() {
    return '--' === $this->ipCountry ? NULL : $this->ipCountry;
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstFourCardNumberDigits() {
    return $this->cc0104;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastFourCardNumberDigits() {
    return $this->cc1316;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\commerce_bangkokbank\Payment\Action\ActionResponse::__construct()
   */
  public function getPaymentMethod() {
    return $this->paymentMethod;
  }

  /**
   * Returns payment remark.
   *
   * @return null|string
   *   Payment remark.
   */
  public function getRemark() {
    return $this->remark;
  }

  /**
   * Returns status of DataFeed settlement.
   *
   * @return bool
   *   A state of check.
   */
  public function isSettled() {
    // Can be "T" or "F".
    return 't' === strtolower($this->settle);
  }

  /**
   * Returns time of settlement.
   *
   * @return null|string
   *   Time of settlement.
   */
  public function getSettleTime() {
    return $this->settleTime;
  }

  /**
   * Returns batch ID of settlement.
   *
   * @return null|string
   *   Batch ID of settlement.
   */
  public function getSettleBatchId() {
    return $this->settleBatchId;
  }

  /**
   * Returns additional merchant reference.
   *
   * @param int $index
   *   Index of additional merchant reference.
   *
   * @return string|null
   *   Additional merchant reference.
   *
   * @see \Drupal\commerce_bangkokbank\Payment\Authorisation\Request::getAdditionalMerchantReference()
   */
  public function getAdditionalMerchantReference($index) {
    if ($index < 1 || $index > 5) {
      throw new \InvalidArgumentException('Index must be between 1 and 5.');
    }

    return $this->{'orderRef' . $index};
  }

  /**
   * Returns time of transaction.
   *
   * @return null|int
   *   Timestamp of the transaction.
   */
  public function getTransactionTime() {
    $time = strtotime($this->txTime);

    // Convert boolean to NULL to be consistent with other methods.
    return FALSE === $time ? NULL : $time;
  }

}
