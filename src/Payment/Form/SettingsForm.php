<?php

namespace Drupal\commerce_bangkokbank\Payment\Form;

use Commerce\Utils\Payment\Form\SettingsFormBase;
use Drupal\commerce_bangkokbank\Payment\Environment;
use Drupal\commerce_bangkokbank\Payment\PaymentTypeInterface;
use Drupal\commerce_bangkokbank\Payment\PaymentLanguageInterface;

/**
 * {@inheritdoc}
 */
class SettingsForm extends SettingsFormBase {

  /**
   * Key to mark newly added currency. Will be replaced further.
   */
  const NEW_CURRENCY_ARRAY_KEY = 'new';
  /**
   * Key to associate currency configuration with.
   */
  const EXISTING_CURRENCY_KEY = 'currency_code';

  /**
   * {@inheritdoc}
   */
  public function form(array &$form, array &$settings) {
    $currency_options = commerce_currency_get_code(TRUE);

    $settings += ['mode' => Environment::TEST];
    $settings += [$settings['mode'] => []];

    $form['mode'] = [
      '#ajax' => TRUE,
      '#type' => 'radios',
      '#title' => t('Mode'),
      '#required' => TRUE,
      '#default_value' => Environment::TEST,
      '#options' => [
        Environment::TEST => t('Test'),
        Environment::LIVE => t('Live'),
      ],
    ];

    $form['payment_type'] = [
      '#type' => 'radios',
      '#title' => t('Payment type'),
      '#required' => TRUE,
      '#description' => t('Note, that "Hold" payment type can be used only if you merchant account support this feature. Contact gateway support to enable it.'),
      '#default_value' => PaymentTypeInterface::NORMAL,
      '#options' => [
        PaymentTypeInterface::NORMAL => t('Normal (sales)'),
        PaymentTypeInterface::HOLD => t('Hold (authorisation only)'),
      ],
    ];

    $form['language'] = [
      '#type' => 'select',
      '#title' => t('Language'),
      '#required' => TRUE,
      '#description' => t('Selected language will be used on payment page.'),
      '#default_value' => PaymentLanguageInterface::ENGLISH,
      '#options' => [
        PaymentLanguageInterface::ENGLISH => t('English'),
        PaymentLanguageInterface::THAI => t('Thai'),
        PaymentLanguageInterface::CHINESE => t('Chinese'),
      ],
    ];

    $form['redirect_timeout'] = [
      '#type' => 'textfield',
      '#title' => t('Redirect timeout'),
      '#description' => t('Number of seconds before automatic redirect after successful payment. Use "0" for disabling.'),
      '#element_validate' => ['element_validate_integer'],
      '#default_value' => 10,
    ];

    switch ($this->getAjaxAction()) {
      case '_add':
        $settings[$settings['mode']]['currencies'][] = [];
        break;

      case '_remove':
        drupal_array_set_nested_value($settings, $this->getAjaxParents(), NULL);
        break;
    }

    foreach ($form['mode']['#options'] as $mode => $label) {
      $form[$mode]['currencies'] = [
        '#tree' => TRUE,
        '#type' => 'vertical_tabs',
        '#access' => $settings['mode'] === $mode,
        '#prefix' => '<h6>' . t('@label-mode currencies', ['@label' => $label]) . '</h6>',
      ];

      foreach (empty($settings[$mode]['currencies']) ? [[]] : $settings[$mode]['currencies'] as $data) {
        // Skip "active_tab" and removed elements.
        if (!is_array($data)) {
          continue;
        }

        if (empty($data['currency_code'])) {
          $title = t('New');
          $key = static::NEW_CURRENCY_ARRAY_KEY;
        }
        else {
          $title = sprintf('%s: %s', $data['currency_code'], $data['merchant_id']);
          $key = $data[static::EXISTING_CURRENCY_KEY];
        }

        $currency = [
          '#type' => 'fieldset',
          '#title' => $title,
        ];

        $currency['merchant_id'] = [
          '#type' => 'textfield',
          '#size' => 5,
          '#title' => t('Merchant ID'),
          '#prefix' => '<div class="currency-info clearfix">',
          '#required' => TRUE,
        ];

        $currency['currency_code'] = [
          '#type' => 'select',
          '#title' => t('Currency code'),
          '#suffix' => '</div>',
          '#options' => $currency_options,
          '#required' => TRUE,
        ];

        $currency['api'] = [
          '#type' => 'fieldset',
          '#title' => t('API'),
        ];

        $currency['api']['username'] = [
          '#type' => 'textfield',
          '#title' => t('Username'),
          '#required' => TRUE,
        ];

        $currency['api']['password'] = [
          '#type' => 'textfield',
          '#title' => t('Password'),
          '#required' => TRUE,
        ];

        $currency['_remove'] = [
          '#type' => 'button',
          '#ajax' => TRUE,
          '#value' => t('Remove'),
          // No need to perform validation on element deletion.
          '#limit_validation_errors' => [],
        ];

        $form[$mode]['currencies'][$key] = $currency;
      }
    }

    $form['_add'] = [
      '#ajax' => TRUE,
      '#type' => 'button',
      '#value' => t('Add currency'),
    ];

    $form['#attached']['css'][] = [
      'data' => '.currency-info > * {float: left; margin-right: 20px !important;}',
      'type' => 'inline',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array &$form, array &$settings) {
    foreach ($form['mode']['#options'] as $mode => $label) {
      $currencies =& $settings[$mode]['currencies'];

      $currencies = array_map(function (array $value) {
        // Delete the value of "_remove" button.
        unset($value['_remove']);

        return $value;
      }, array_filter($currencies, function ($value) {
        // Allow only configured currencies and skip empty.
        // Inside of "$value" might be a string - current tab indicator.
        return is_array($value) && !empty($value['merchant_id']);
      }));

      // New currency was added. Replace key-marker by correct key.
      if (isset($currencies[static::NEW_CURRENCY_ARRAY_KEY])) {
        $new = $currencies[static::NEW_CURRENCY_ARRAY_KEY];

        $currencies[$new[static::EXISTING_CURRENCY_KEY]] = $new;
        unset($currencies[static::NEW_CURRENCY_ARRAY_KEY]);
      }
    }

    // Delete the value of "_add" button.
    unset($settings['_add']);
  }

}
