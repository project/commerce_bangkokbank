<?php

namespace Drupal\commerce_bangkokbank\Payment\Form;

use Commerce\Utils\Payment\Form\RedirectFormBase;
use Commerce\Utils\Payment\Authorisation\RequestBase;
use Drupal\commerce_bangkokbank\Entity\PaymentWatcher;

/**
 * {@inheritdoc}
 *
 * @method \Drupal\commerce_bangkokbank\Payment\Authorisation\Response getResponse()
 */
class RedirectForm extends RedirectFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(RequestBase $request, array &$form, array &$form_state) {
    PaymentWatcher::createForOrder($request->getOrder()->value(), $request->getPaymentMethod()['method_id']);
  }

  /**
   * {@inheritdoc}
   */
  public function cancel() {
    $this->response();

    throw new \RuntimeException(t('Payment has been cancelled or failed.'));
  }

  /**
   * {@inheritdoc}
   */
  public function response() {
    $response = $this->getResponse();
    $watcher = PaymentWatcher::loadForOrder($response->getOrder()->value(), $response->getTransaction()->getPaymentMethodName());

    // No need to check payment status for the non-existent watcher.
    if (!$watcher->isNew()) {
      $watcher->checkPaymentStatus();
    }
  }

}
