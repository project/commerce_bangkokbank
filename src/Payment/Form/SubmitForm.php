<?php

namespace Drupal\commerce_bangkokbank\Payment\Form;

use Commerce\Utils\Payment\Form\SubmitFormBase;

/**
 * {@inheritdoc}
 */
class SubmitForm extends SubmitFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array &$form, array &$values, array &$checkout_pane) {
    $payment_method = $this->getPaymentMethod();
    $order_currency = $this->getOrder()->commerce_order_total->currency_code->value();
    $settings = $payment_method['settings'][$payment_method['settings']['mode']];

    if (empty($settings['currencies'][$order_currency])) {
      $form['#error'] = t('Order currency is @order_currency and it is not allowed for usage in @payment_method payment.', [
        '@order_currency' => $order_currency,
        '@payment_method' => $payment_method['title'],
      ]);
    }
    else {
      $form[COMMERCE_BANGKOKBANK_PAYMENT_METHOD] = [
        '#tree' => TRUE,
      ];

      foreach (['merchant_id', 'currency_code'] as $key) {
        $form[COMMERCE_BANGKOKBANK_PAYMENT_METHOD][$key] = [
          '#type' => 'hidden',
          '#value' => $settings['currencies'][$order_currency][$key],
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array $form, array &$values) {
    if (!empty($form['#error'])) {
      throw new \RuntimeException($form['#error']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array $form, array &$values, array &$balance) {
    $this->getOrder()->value()->data[COMMERCE_BANGKOKBANK_PAYMENT_METHOD] = $values[COMMERCE_BANGKOKBANK_PAYMENT_METHOD];
  }

}
