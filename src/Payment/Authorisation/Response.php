<?php

namespace Drupal\commerce_bangkokbank\Payment\Authorisation;

use Commerce\Utils\Payment\Authorisation\ResponseBase;
use Drupal\commerce_bangkokbank\Payment\Transaction\Payment as PaymentTransaction;

/**
 * {@inheritdoc}
 *
 * @method PaymentTransaction getTransaction()
 */
class Response extends ResponseBase {

  use Signature;

  /**
   * {@inheritdoc}
   */
  public function __construct(PaymentTransaction $transaction, array $payment_method) {
    $query = [];

    // Using "$_GET" all "." will be converted to "_" and all "+" - to " ".
    /* @link http://stackoverflow.com/a/68742 */
    foreach (explode('&', rawurldecode($_SERVER['QUERY_STRING'])) as $pair) {
      list($name, $value) = explode('=', $pair);

      $query[$name] = $value;
    }

    // Query will contain:
    // @code
    // [
    //   'Ref' => 'TH-LW-10',
    //   'Ref1' => 'ssavov+9@propeople.dk',
    //   'Ref2' => '1498803193',
    //   'Ref3' => '',
    //   'Ref4' => '',
    //   'Ref5' => '',
    // ]
    // @endcode
    $this->data = drupal_get_query_parameters($query);

    $order_wrapper = $transaction->getOrder();

    /* @see \Drupal\commerce_bangkokbank\Payment\Authorisation\Request::__construct() */
    foreach ([
      'Ref' => 'order_number',
      'Ref1' => 'mail',
      'Ref2' => 'created',
    ] as $response_property => $order_property) {
      if (empty($this->data[$response_property])) {
        throw new \UnexpectedValueException(t('Empty or incorrect response from Bangkok Bank has been received.'));
      }
      else {
        $order_value = $order_wrapper->{$order_property}->value();
        $response_value = $this->data[$response_property];

        // Do not use strict comparision.
        if ($order_value != $response_value) {
          throw new \UnexpectedValueException(t('The wrong response has been received from Bangkok Bank. Property "@order_property" of the order (@order_value) represents the "@response_property" property from the response (@response_value), but their values differ.', [
            '@order_value' => $order_value,
            '@order_property' => $order_property,
            '@response_value' => $response_value,
            '@response_property' => $response_property,
          ]));
        }
      }
    }

    parent::__construct($transaction, $payment_method);
  }

}
