<?php

namespace Drupal\commerce_bangkokbank\Payment\Authorisation;

/**
 * Payment signature.
 */
trait Signature {

  /**
   * {@inheritdoc}
   */
  protected function getSignature() {
    return '';
  }

}
