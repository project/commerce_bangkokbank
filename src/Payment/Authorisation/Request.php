<?php

namespace Drupal\commerce_bangkokbank\Payment\Authorisation;

use Commerce\Utils\Payment\Authorisation\RequestBase;
use Drupal\commerce_bangkokbank\Payment\Environment;
use Drupal\commerce_bangkokbank\Payment\PaymentTypeInterface;
use Drupal\commerce_bangkokbank\Payment\PaymentLanguageInterface;

/**
 * {@inheritdoc}
 */
class Request extends RequestBase {

  use Signature;

  /**
   * {@inheritdoc}
   */
  public function __construct(\stdClass $order, array $payment_method) {
    if (empty($payment_method['settings'])) {
      throw new \UnexpectedValueException(t('You are not configured Adyen payment gateway!'));
    }

    parent::__construct($order, $payment_method);
    // Wrapper can be obtained only after it'll be constructed.
    $order_wrapper = $this->getOrder();

    // Payment fields.
    $this->setMerchantReference($order->order_number);
    /* @see \Drupal\commerce_bangkokbank\Payment\Form\SubmitForm */
    $this->setMerchantAccount($order->data[COMMERCE_BANGKOKBANK_PAYMENT_METHOD]['merchant_id']);
    // Currency code must be set before amount!
    $this->setCurrencyCode($order_wrapper->commerce_order_total->currency_code->value());
    $this->setPaymentAmount($order_wrapper->commerce_order_total->amount->value());
    $this->setCountryCode($order_wrapper->commerce_customer_billing->commerce_customer_address->country->value());
    $this->setPaymentType($payment_method['settings']['payment_type']);
    $this->setShopperLocale($payment_method['settings']['language']);
    $this->setShopperEmail($order->mail);
    $this->setShopperIp(ip_address());
    // Required field.
    $this->setRemark(t('Payment for @reference made by @shopper_mail from @shopper_ip.', [
      '@reference' => $this->getMerchantReference(),
      '@shopper_mail' => $this->getShopperEmail(),
      '@shopper_ip' => $this->getShopperIp(),
    ]));

    // Store customer email and order creation timestamp to identify it in
    // future. Do not use UID, because order can be created by anonymous user.
    // Also, these values will be used for comparison during API requests.
    /* @see \Drupal\commerce_bangkokbank\Entity\PaymentWatcher::checkPaymentStatus() */
    /* @see \Drupal\commerce_bangkokbank\Payment\Authorisation\Response::__construct() */
    $this->setAdditionalMerchantReference($order->mail);
    $this->setAdditionalMerchantReference($order->created);

    if ($payment_method['settings']['redirect_timeout'] > 0) {
      $this->setRedirectTimeout($payment_method['settings']['redirect_timeout']);
    }

    // These values must be never changed.
    $this->data['failUrl'] = $this->data['cancelUrl'] = $this->returnLink('back');
    $this->data['successUrl'] = $this->returnLink('return');
  }

  /**
   * Set merchant account.
   *
   * @param int $merchant_id
   *   The merchant account you want to process this payment with.
   */
  public function setMerchantAccount($merchant_id) {
    $this->data['merchantId'] = (int) $merchant_id;
  }

  /**
   * Get merchant account.
   *
   * @return int
   *   Merchant account.
   */
  public function getMerchantAccount() {
    return $this->data['merchantId'];
  }

  /**
   * Set currency code.
   *
   * Currency codes must be numeric according to specification of the gateway.
   * In payment configuration user is allowed to associate human-understandable
   * code with its numeric representation. Also, only one currency is available
   * to be used for every particular merchant ID.
   *
   * @param string|int $currency_code
   *   Currency code.
   */
  public function setCurrencyCode($currency_code) {
    // Give it a try to transform currency code to currency ID, required by
    // iPay PayGate.
    if (!is_numeric($currency_code)) {
      $settings = $this->getPaymentMethod()['settings'];

      if (empty($settings[$settings['mode']]['currencies'][$currency_code])) {
        throw new \InvalidArgumentException(t('@currency_code currency is not configured in payment method settings.', [
          '@currency_code' => $currency_code,
        ]));
      }

      // No need to check currency code existence because in settings we
      // are requiring to choose only one of enabled.
      $currency_code = commerce_currencies()[$currency_code]['numeric_code'];
    }

    $this->data['currCode'] = $currency_code;
  }

  /**
   * Get currency code.
   *
   * @return string
   *   Currency code.
   */
  public function getCurrencyCode() {
    return $this->data['currCode'];
  }

  /**
   * Set amount of a payment.
   *
   * @param int $payment_amount
   *   Payment amount. Specified in minor units.
   *
   * @throws \InvalidArgumentException
   */
  public function setPaymentAmount($payment_amount) {
    if (empty($this->data['currCode'])) {
      throw new \InvalidArgumentException(t('You must set currency code before setting the price!'));
    }

    $this->data['amount'] = commerce_currency_amount_to_decimal($payment_amount, $this->getOrder()->commerce_order_total->currency_code->value());
  }

  /**
   * Get amount of a payment.
   *
   * @return string
   *   Amount of a payment.
   */
  public function getPaymentAmount() {
    return $this->data['amount'];
  }

  /**
   * Set merchant reference.
   *
   * @param string $merchant_reference
   *   Merchant reference.
   *
   * @example
   * $payment->setMerchantReference('DE-LW-2013');
   */
  public function setMerchantReference($merchant_reference) {
    $this->data['orderRef'] = $merchant_reference;
  }

  /**
   * Get merchant reference.
   *
   * @return string
   *   Merchant reference.
   */
  public function getMerchantReference() {
    return $this->data['orderRef'];
  }

  /**
   * Set additional merchant reference.
   *
   * @param string $merchant_reference
   *   Custom merchant reference.
   */
  public function setAdditionalMerchantReference($merchant_reference) {
    static $counter = 1;

    if ($counter > 5) {
      throw new \RuntimeException('You can set up to 5 additional merchant references.');
    }

    $this->data['orderRef' . $counter++] = $merchant_reference;
  }

  /**
   * Returns additional merchant reference.
   *
   * @param int $index
   *   Index of additional merchant reference.
   *
   * @return string
   *   Additional merchant reference.
   */
  public function getAdditionalMerchantReference($index) {
    if ($index < 1 || $index > 5) {
      throw new \InvalidArgumentException('Index must be between 1 and 5.');
    }

    return isset($this->data['orderRef' . $index]) ? $this->data['orderRef' . $index] : '';
  }

  /**
   * Set shopper locale.
   *
   * @param string $language
   *   Case insensitive language identifier:
   *   - "t" or "th" - for Thai;
   *   - "e" or "en" - for English;
   *   - "x", "zh", "zh-hans" or "zh-hant" - for Chinese.
   */
  public function setShopperLocale($language) {
    $language = strtoupper($language);

    switch ($language) {
      case 'TH':
        $language = PaymentLanguageInterface::THAI;
        break;

      case 'EN':
        $language = PaymentLanguageInterface::ENGLISH;
        break;

      case 'ZH':
      case 'ZH-HANS':
      case 'ZH-HANT':
        $language = PaymentLanguageInterface::CHINESE;
        break;
    }

    $this->data['lang'] = $language;
  }

  /**
   * Get shopper locale.
   *
   * @return string
   *   Shopper locale.
   *
   * @see \Drupal\commerce_bangkokbank\Payment\PaymentLanguageInterface
   */
  public function getShopperLocale() {
    return $this->data['lang'];
  }

  /**
   * Set payment type.
   *
   * @param string $payment_type
   *   One of payment types, defined in "PaymentTypeInterface".
   *
   * @see \Drupal\commerce_bangkokbank\Payment\PaymentTypeInterface
   */
  public function setPaymentType($payment_type) {
    $payment_type = strtoupper($payment_type);

    if (!in_array($payment_type, [PaymentTypeInterface::NORMAL, PaymentTypeInterface::HOLD])) {
      throw new \InvalidArgumentException(t('"@payment_type" payment type is not supported.', [
        '@payment_type' => $payment_type,
      ]));
    }

    $this->data['payType'] = $payment_type;
  }

  /**
   * Get payment type.
   *
   * @return string
   *   Payment type ("N" or "H").
   */
  public function getPaymentType() {
    return $this->data['payType'];
  }

  /**
   * Set remark (will not be shown on transaction page).
   *
   * @param string $remark
   *   Additional data to store, 200 symbols maximum.
   */
  public function setRemark($remark) {
    $this->data['remark'] = $remark;
  }

  /**
   * Get remark.
   *
   * @return string
   *   Payment remark.
   */
  public function getRemark() {
    return $this->data['remark'];
  }

  /**
   * Set country code.
   *
   * @param string $country_code
   *   Country code.
   *
   * @see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
   */
  public function setCountryCode($country_code) {
    $this->data['ipCountry'] = strtoupper($country_code);
  }

  /**
   * Get country code.
   *
   * @return string
   *   Country code.
   */
  public function getCountryCode() {
    return $this->data['ipCountry'];
  }

  /**
   * Set shopper IP address.
   *
   * @param string $shopper_ip
   *   Shopper IP address.
   */
  public function setShopperIp($shopper_ip) {
    $this->data['sourceIp'] = $shopper_ip;
  }

  /**
   * Get shopper IP address.
   *
   * @return string
   *   Shopper IP address.
   */
  public function getShopperIp() {
    return $this->data['sourceIp'];
  }

  /**
   * Set shopper email.
   *
   * @param string $shopper_email
   *   The email address of a shopper.
   */
  public function setShopperEmail($shopper_email) {
    $this->data['holderEmail'] = $shopper_email;
  }

  /**
   * Get shopper email.
   *
   * @return string
   *   Shopper email.
   */
  public function getShopperEmail() {
    return $this->data['holderEmail'];
  }

  /**
   * Set redirect timeout.
   *
   * @param int $timeout
   *   Number of seconds before redirect to success/cancel/fail URL.
   */
  public function setRedirectTimeout($timeout) {
    $this->data['redirect'] = (int) $timeout;
  }

  /**
   * Get redirect timeout.
   *
   * @return int
   *   Number of seconds before redirect.
   */
  public function getRedirectTimeout() {
    return $this->data['redirect'];
  }

  /**
   * Set device mode (type of payment screen).
   *
   * @param string $mode
   *   Device mode. Use one "Environment::PC" or "Environment::MOBILE".
   */
  public function setDeviceMode($mode) {
    if (!in_array($mode, [Environment::PC, Environment::MOBILE])) {
      throw new \InvalidArgumentException(t('Incorrect device mode.'));
    }

    $this->data['deviceMode'] = $mode;
  }

  /**
   * Get device mode.
   *
   * @return string
   *   Device mode.
   */
  public function getDeviceMode() {
    return $this->data['deviceMode'];
  }

  /**
   * Get gateway environment.
   *
   * @return Environment
   *   Gateway environment.
   */
  public function getEnvironment() {
    return Environment::createFromSettings($this->getPaymentMethod()['settings']);
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoint() {
    return $this->getEnvironment()->getFormUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function signRequest() {
  }

}
