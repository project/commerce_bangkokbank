<?php

namespace Drupal\commerce_bangkokbank\Payment;

/**
 * Lists payment statuses.
 *
 * All values of constants MUST BE in lowercase!
 *
 * @see \Commerce\Utils\Transaction::setRemoteStatus()
 * @see \Drupal\commerce_bangkokbank\Payment\Action\QueryResponse::getPaymentStatus()
 */
interface PaymentStatusInterface {

  /**
   * {@inheritdoc}
   *
   * Transaction is accepted by card-issuing bank.
   */
  const ACCEPTED = 'accepted';
  /**
   * {@inheritdoc}
   *
   * Transaction accepted with partial chargeback or partial refund involved.
   */
  const ACCEPTED_ADJ = 'accepted_adj';
  /**
   * {@inheritdoc}
   *
   * Transaction is authorized by card-issuing bank with approval code. Awaits
   * for capture.
   */
  const AUTHORISED = 'authorized';
  /**
   * {@inheritdoc}
   *
   * Transaction is cancelled by cardholder.
   */
  const CANCELLED = 'cancelled';
  /**
   * {@inheritdoc}
   *
   * The capturing process is pending.
   */
  const CAPTURING = 'capturing';
  /**
   * {@inheritdoc}
   *
   * Transaction amount is fully chargeback by the cardholder or card-issuing
   * bank.
   */
  const CHARGEBACK = 'chargeback';
  /**
   * {@inheritdoc}
   *
   * Transaction amount is partially chargeback by the cardholder or
   * card-issuing bank.
   */
  const PARTIAL_CHARGEBACK = 'partial chargeback';
  /**
   * {@inheritdoc}
   *
   * Requisition of partial refund is successfully processed by Merchant.
   */
  const PARTIAL_REFUNDED = 'partial refunded';
  /**
   * {@inheritdoc}
   *
   * Transaction is requesting authorization from issuing bank.
   */
  const PENDING = 'pending';
  /**
   * {@inheritdoc}
   *
   * 3D secure process is pending.
   */
  const PENDING_3D = 'pending_3d';
  /**
   * {@inheritdoc}
   *
   * Requisition of full refund is successfully processed by Merchant.
   */
  const REFUNDED = 'refunded';
  /**
   * {@inheritdoc}
   *
   * Transaction is rejected, for some reasons.
   */
  const REJECTED = 'rejected';
  /**
   * {@inheritdoc}
   *
   * Requisition of partial refund from merchant is under processing.
   */
  const REQUESTPARTIALREFUND = 'requestpartialrefund';
  /**
   * {@inheritdoc}
   *
   * Requisition of full refund from merchant is under processing.
   */
  const REQUEST_REFUND = 'request refund';
  /**
   * {@inheritdoc}
   *
   * Transaction had been chargeback before but now successfully appeal by the
   * merchant.
   */
  const REVERSAL_CB = 'reversal-cb';
  /**
   * {@inheritdoc}
   *
   * Withdraw the void request by merchant.
   */
  const REVERSAL_VOID = 'reversal void';
  /**
   * {@inheritdoc}
   *
   * Withdraw the authorization request by merchant.
   */
  const REVERSE_AUTH = 'reverse auth';
  /**
   * {@inheritdoc}
   *
   * Transaction is void by merchant.
   */
  const VOIDED = 'voided';

}
