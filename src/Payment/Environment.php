<?php

namespace Drupal\commerce_bangkokbank\Payment;

/**
 * Payment environment, depending on configuration.
 */
class Environment {

  const URL = 'bangkokbank.com';
  const TEST = 'test';
  const LIVE = 'live';
  const PC = 'pc';
  const MOBILE = 'mobile';

  private $prefix = 'ipay';

  /**
   * Environment constructor.
   *
   * @param int $environment
   *   Environment type. Must be one of class constants ("TEST" or "LIVE").
   */
  public function __construct($environment) {
    if (!in_array($environment, [static::TEST, static::LIVE], TRUE)) {
      throw new \InvalidArgumentException('You may select between test and live environments.');
    }

    // "psipay" or "ipay".
    if (static::TEST === $environment) {
      $this->prefix = 'ps' . $this->prefix;
    }
  }

  /**
   * Instantiate environment from settings of payment method.
   *
   * @param array $settings
   *   Payment method settings.
   *
   * @return static
   */
  public static function createFromSettings(array $settings) {
    if (!isset($settings['mode'])) {
      $settings['mode'] = static::TEST;
    }

    return new static($settings['mode']);
  }

  /**
   * Returns URL to home page of service.
   *
   * @return string
   *   URL to service.
   */
  public function getHomeUrl() {
    return 'http://www.' . static::URL;
  }

  /**
   * Returns base, environment-specific, URL.
   *
   * @return string
   *   Base URL.
   */
  public function getBaseUrl() {
    return 'https://' . $this->prefix . '.' . static::URL;
  }

  /**
   * Returns URL for API calls.
   *
   * @return string
   *   URL for API calls.
   */
  public function getApiUrl() {
    return $this->getBaseUrl() . '/b2c/eng/merchant/api/orderApi.jsp';
  }

  /**
   * Returns URL to send payment requests to.
   *
   * @return string
   *   URL for payment requests.
   */
  public function getFormUrl() {
    return $this->getBaseUrl() . '/b2c/eng/payment/payForm.jsp';
  }

}
