<?php

namespace Drupal\commerce_bangkokbank\Payment;

/**
 * Interface for describing objects containing card information.
 */
interface PaymentCardDataInterface {

  /**
   * Return first four digits of card number.
   *
   * @return string
   *   First four digits of card number.
   */
  public function getFirstFourCardNumberDigits();

  /**
   * Return last four digits of card number.
   *
   * @return string
   *   Last four digits of card number.
   */
  public function getLastFourCardNumberDigits();

  /**
   * Returns payment method computed from a number of the card.
   *
   * @return string
   *   Name of payment method in lowercase.
   */
  public function getPaymentMethod();

}
