<?php

namespace Drupal\commerce_bangkokbank;

use Drupal\commerce_bangkokbank\Payment\PaymentBankCodesInterface;

/**
 * Converts bank's primary and secondary return codes into human text.
 *
 * All information described in this class are taken from official
 * documentation, provided by the bank. Also you can find it in "docs"
 * directory within this module, in the ""
 */
class BankReturnCodesDescription {

  const PRIMARY = [
    [0, 'Success'],
    [1, 'Rejected by Payment Bank'],
    [3, 'Rejected due to Payer Authentication Failure (3D)'],
    [5, 'Rejected due to DCC Processing transaction Error'],
    [-1, 'Rejected due to Input Parameters Incorrect'],
    [-2, 'Rejected due to Server Access Error'],
    [-8, 'Rejected due to iPay Internal/Fraud Prevention Checking'],
    [-9, 'Rejected by Host Access Error'],
  ];

  const COMBINATIONS = [
    // Bank response codes.
    [1, 1, 'Refer to card issuer'],
    [1, 3, 'Invalid merchant'],
    [1, 4, 'Pick up card (frozen card)'],
    [1, 5, 'Do not honor'],
    [1, 7, 'Fraud card'],
    [1, 12, 'Invalid transaction'],
    [1, 13, 'Invalid amount'],
    [1, 14, 'Invalid account number'],
    [1, 19, 'Re-enter transaction'],
    [1, 25, 'Not a system card'],
    [1, 30, 'Format error'],
    [1, 41, 'Pick up card (lost card)'],
    [1, 43, 'Pick up card (stolen card)'],
    [1, 51, 'Not sufficient funds'],
    [1, 54, 'Expired card'],
    [1, 58, 'Transaction not permitted to terminal'],
    [1, 61, 'Activity amount limit exceeded'],
    [1, 62, 'Restricted card'],
    [1, 70, 'Sales amount out of range'],
    [1, 71, 'Charge-to mismatch'],
    [1, 72, 'Product expire'],
    [1, 73, 'Payment term mismatch'],
    [1, 74, 'Product not found'],
    [1, 76, 'Invalid Item Price'],
    [1, 91, 'Issuer or switch inoperative'],
    [1, 96, 'System malfunction or errors in fields'],
    [1, 99, 'Host access error'],
    // iPay response codes.
    [-8, 978, 'Bank settlement in progress'],
    [-8, 2001, 'Blacklist card by system'],
    [-8, 2002, 'Blacklist card by merchant'],
    [-8, 2003, 'Black IP by system'],
    [-8, 2004, 'Black IP by merchant'],
    [-8, 2006, 'Same card used more than 6 times a day'],
    [-8, 2007, 'Duplicate merchant reference number'],
    [-8, 2008, 'Empty merchant reference number'],
    [-8, 2014, 'High risk country'],
    [-8, 2016, 'Same payer IP attempted more than pre-defined number a day'],
    [-8, 2017, 'Invalid card number'],
    [-8, 2018, 'Multi-card attempt'],
    [-8, 2019, 'Incorrect payment information'],
    [-8, 2021, 'Country not match'],
    // Other response codes.
    [0, 0, 'Successful payment'],
    [3, 1003, 'Payer authentication fail (Verified by Visa)'],
    [3, 1004, 'Payer authentication fail (MasterCard SecureCode)'],
    [3, 2008, 'Payer authentication fail (lack of VbV)'],
    [3, 2010, 'Payer authentication fail (SecureCode)'],
    [3, 3001, 'Payer authentication fail (J / Secure IDs)'],
    [3, 3004, 'Payer authentication fail (unknown)'],
    [3, 3005, 'Payer authentication fail (unknown)'],
    [3, 3007, 'Payer authentication fail (unknown)'],
    [3, 9999, 'Payer authentication fail (unknown)'],
    [5, 9999, 'The currency is not a DCC currency'],
    [-2, -2, 'Server access error'],
    [-9, -9, 'Host access error'],
  ];

  /**
   * An object containing bank's return codes.
   *
   * @var \Drupal\commerce_bangkokbank\Payment\PaymentBankCodesInterface
   */
  private $payment;
  /**
   * List of primary codes descriptions.
   *
   * @var string[]
   */
  private $primary = [];
  /**
   * List of descriptions of known combinations of primary and secondary codes.
   *
   * @var string[]
   */
  private $combinations = [];

  /**
   * BankReturnCodesDescription constructor.
   *
   * @param \Drupal\commerce_bangkokbank\Payment\PaymentBankCodesInterface $payment
   *   An object containing bank's return codes.
   */
  public function __construct(PaymentBankCodesInterface $payment) {
    $this->payment = $payment;

    foreach (static::PRIMARY as list($primary, $description)) {
      $this->primary[$primary] = $description;
    }

    foreach (static::COMBINATIONS as list($primary, $secondary, $description)) {
      $this->combinations[$primary + $secondary] = $description;
    }
  }

  /**
   * Returns bank's codes description.
   *
   * @return string
   *   Bank's codes description.
   */
  public function __toString() {
    $primary = $this->payment->getPrimaryBankReturnCode();
    $secondary = $this->payment->getSecondaryBankReturnCode();
    $total = $primary + $secondary;

    if (isset($this->combinations[$total])) {
      return $this->combinations[$total];
    }
    elseif (isset($this->primary[$primary])) {
      return $this->primary[$primary];
    }

    watchdog(COMMERCE_BANGKOKBANK_PAYMENT_METHOD, 'A combination of primary (@primary) and secondary (@secondary) bank return codes is unknown.', [
      '@primary' => $primary,
      '@secondary' => $secondary,
    ], WATCHDOG_WARNING);

    return 'Unknown primary and secondary codes combination';
  }

}
